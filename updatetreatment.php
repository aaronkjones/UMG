<?php

// Creds for MySQL connection
require_once 'creds.php';

// Session management
session_start();
$_SESSION['message'] = '';

// Check user's info for session hijack prevention
if ($_SESSION['check'] != hash('ripemd128', $_SERVER['REMOTE_ADDR'] .
$_SERVER['HTTP_USER_AGENT']))
{
  header("location: index.php");
}
else {

  // Establish mysql connection
  $db = new mysqli($db_host, $db_user, $db_pass, $db_database);

  // Connection error handling
  if ($db->connect_error)
  {
    die("<h2>Oh noooo...Connection to database failed!</h2> " . $db->connect_error);
  }

  // Make sure the form is being posted
  if($_SERVER['REQUEST_METHOD'] == 'POST')
  {
    $treatmentID = $db->real_escape_string($_POST['treatmentID']);
    $treat_name = $db->real_escape_string($_POST['treat_name']);
    $start_date = $db->real_escape_string($_POST['start_date']);
    $end_date = $db->real_escape_string($_POST['end_date']);
    $start_time = $db->real_escape_string($_POST['start_time']);
    $end_time = $db->real_escape_string($_POST['end_time']);
    $physicianID = $db->real_escape_string($_POST['physicianID']);
    $patientID = $db->real_escape_string($_POST['patientID']);
    $notes = $db->real_escape_string($_POST['notes']);

    $sql = "UPDATE treatments SET treat_name = $treat_name, start_date = $start_date, end_date = $end_date, start_time = $start_time, end_time = $end_time, physicianID = $physicianID, patientID = $patientID, notes = $notes" or die($db->error);
    //
    if ($db->query($sql) === TRUE) {
      $_SESSION['message'] = 'Record update successful';
    } else {
      $_SESSION['message'] = "There was an error adding the treatment<br>" . mysqli_error($db);
    }
  }
}
?>

    <!DOCTYPE html>
    <html lang="en">
    <head>

      <!-- Favicon -->
      <link rel="icon" href="images/favicon.ico" type="image/x-icon">

      <meta charset="UTF-8">

      <!-- If IE use the latest rendering engine -->
      <meta http-equiv="X-UA-Compatible" content="IE=edge">

      <!-- Set the page to the width of the device and set the zoon level -->
      <meta name="viewport" content="width = device-width, initial-scale = 1">
      <title>Uinta Medical Group</title>

      <!-- Bootstrap styling -->
      <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
      <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
      <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

      <!-- Fontawesome awesomeness -->
      <script src="https://use.fontawesome.com/7b7005c99f.js"></script>

      <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
      <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

      <!-- Custom styles for this template -->
      <link href="https://getbootstrap.com/examples/sticky-footer-navbar/sticky-footer-navbar.css" rel="stylesheet">

      <!-- Custom styles for this template -->
      <link href="css/sticky-footer-navbar.css" rel="stylesheet">
      <link href="css/fancy.css" rel="stylesheet">

      <!-- Custom javascript -->
      <script src="slide.js"></script>

    </head>
    <body>

      <!-- Top navbar -->
      <?php require_once('navbar.php');?>

      <div id="content-wrapper">

        <div class="container" style="padding: 0px; margin-bottom:50px">
          <h1 class="well">Update Treatment</h1>
          <div class="col-lg-12 well">
            <div class="row">

              <!-- Form -->
              <form action="addtreatment.php" method="post">
                <div class="col-sm-12">
                  <!-- Row -->
                  <div class="row">
                  <div class="col-sm-4 form-group">
                    <label>Treatment ID</label>
                    <select class="form-control" name="treat_name" required>
                      <option value = "">---Select---</option>
                      <?php
                      // Get patients ID, first name, last name, and sort alphabetically by last name
                      $query = mysqli_query($db, "SELECT `start_date`, `treatmentID` FROM `treatments` ORDER BY 'start_date'");
                      while ( $d=mysqli_fetch_assoc($query)) {
                        echo "<option value='"  . $d['treatmentID'] . "'>" . $d['treatmentID'] . ", " . $d['start_date'] . "</option>";
                      }
                      ?>
                    </select>
                  </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-6 form-group">
                      <label>Treatment Name</label>
                      <input type="text" placeholder="Allergies" class="form-control" name="treat_name">
                    </div>
                  </div>


                  <hr style="border-color:#e5e5e5">

                  <!-- Row -->
                  <div class="row">
                    <div class="col-sm-4 form-group">
                      <label>Start Date</label>
                      <input type="date" class="form-control" name="start_date">
                    </div>
                    <div class="col-sm-4 form-group">
                      <label>Start Time</label>
                      <input type="text" placeholder="Enter start time" class="form-control" name="start_time">
                    </div>
                  </div>
                  <!-- Row -->
                  <div class="row">
                    <div class="col-sm-4 form-group">
                      <label>End Date</label>
                      <input type="date" class="form-control" name="end_date">
                    </div>
                    <div class="col-sm-4 form-group">
                      <label>End Time</label>
                      <input type="text" placeholder="Enter end time" class="form-control" name="end_time">
                    </div>
                  </div>

                  <hr style="border-color:#e5e5e5">


                  <div class="row">

                    <!-- treatment selection -->
                    <div class="col-sm-4 form-group">
                      <label>Physician</label>
                      <select class="form-control" name="physician" required>
                        <option value = "">---Select---</option>
                        <?php
                        // Get patients ID, first name, last name, and sort alphabetically by last name
                        $query = mysqli_query($db, "SELECT `physicianID`,`fname`,`lname` FROM `physicians` ORDER BY 'lname'");
                        while ( $d=mysqli_fetch_assoc($query)) {
                          echo "<option value='".$d['physicianID']."'>".$d['lname'].", ".$d['fname']."</option>";
                        }
                        ?>
                      </select>
                    </div>

                    <!-- patient selection -->
                    <div class="col-sm-4 form-group">
                      <label>Patient</label>
                      <select class="form-control" name="patient">
                        <option value = "">---Select---</option>
                        <?php
                        // Get patients ID, first name, last name, and sort alphabetically by last name
                        $query = mysqli_query($db, "SELECT `patientID`,`fname`,`lname` FROM `patients` ORDER BY 'lname'");
                        while ( $d=mysqli_fetch_assoc($query)) {
                          echo "<option value='".$d['patientID']."'>".$d['lname'].", ".$d['fname']."</option>";
                        }
                        ?>
                      </select>
                    </div>
                  </div>

                  <!-- Row -->
                  <div class="row">
                    <div class="col-sm-6 form-group">
                      <label>Notes
                        <textarea name="notes" rows="6" cols="50" placeholder="Notes"></textarea>
                      </label>
                    </div>
                  </div>

                  <div class="container" style="padding: 0px">
                    <h2><?php echo $_SESSION['message'];?><br></h2>
                  </div>
                  <hr style="border-color:#e5e5e5">
                  <button type="submit" class="btn btn-primary">Submit</button>
                  <button type="submit" class="btn btn-danger pull-right">Delete</button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </body>
    </div>

    <!-- Footer -->
    <?php require_once('footer.php');?>
    <?php mysqli_close($db);?>
    </html>
