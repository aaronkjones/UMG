<!-- Fancy Nav Bar -->
<nav class="navbar navbar-inverse navbar-default navbar-statc-top" style="margin-bottom:0px; border-radius:0px">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
      </button>
      <a class="navbar-brand" href="index.php"><i class="fa fa-stethoscope" aria-hidden="true"></i></a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li><a href="index.php">Home</a></li>

        <?php
        if(isset($_SESSION['usertype']))
        {
          $usertype = $_SESSION['usertype'];
          if($usertype === 'patients')
          {
            echo '<li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">View <span class="caret"></span></a>
            <ul class="dropdown-menu">
            <li><a href="viewphysicians.php">View Physicians</a></li>
            <li><a href="viewtreatments.php">View Treatments</a></li>
            </ul>
            </li>';
          }
          else
          {
            echo '
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">View <span class="caret"></span></a>
            <ul class="dropdown-menu">
            <li><a href="viewpatients.php">View Patients</a></li>
            <!-- Visibile if user is logged in and is a patient -->
            <li><a href="viewphysicians.php">View Physicians</a></li>
            <li><a href="viewtreatments.php">View Treatments</a></li>
            </ul>
            </li>';
          }

          if(isset($usertype) && $usertype === 'admins')
          {
            echo '<li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Add <span class="caret"></span></a>
            <ul class="dropdown-menu">
            <li><a href="addpatient.php">Add Patient</a></li>
            <li><a href="addphysician.php">Add Physician</a></li>
            </ul>
            </li>

            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Update <span class="caret"></span></a>
            <ul class="dropdown-menu">
            <li><a href="updatepatient.php">Update Patient</a></li>
            <li><a href="updatephysician.php">Update Physician</a></li>
            <li><a href="updatetreatment.php">Update Treatment</a></li>
            </ul>
            </li>';
          }
          elseif($usertype === 'physicians')
          {
            echo '<li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Add <span class="caret"></span></a>
            <ul class="dropdown-menu">
            <li><a href="addtreatment.php">Add Treatment</a></li>
            </ul>
            </li>';
          }
          else echo '';
        }
        ?>

        <li><a href="feedback.php">Feedback</a></li>
        <li><a href="http://childsplaycharity.org/">Donations</a></li>
        <li><a href="publicrelations.php">Public Relations</a></li>

      </ul>

      <ul class="nav navbar-nav navbar-right">
        <li>
          <?php
          if (isset($_SESSION['loggedin']) && $_SESSION['loggedin'] == true) {
            echo '<li><a href="#">'; echo ($_SESSION['username']); echo '</a></li>';
          }
          else
          {
            echo '
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Signin <span class="caret"></span></a>
            <ul class="dropdown-menu">
            <li><a href="signin.php?type=patients">Patient</a></li>
            <li><a href="signin.php?type=physicians">Physician</a></li>
            <li><a href="signin.php?type=admins">Admin</a></li>
            </ul>
            </li>';
          }
          ?>

          <?php
          if($_SESSION['loggedin'] == true)
          {
            echo '<li><a href="signout.php">Signout</a></li>';
          }
          else
          echo '';
          ?>
        </ul>
        <form class="navbar-form navbar-right">
          <div class="form-group">
            <input type="text" class="form-control input-sm" placeholder="Search">
          </div>
          <button type="submit" class="btn btn-sm btn-primary">Submit</button>
        </form>
      </div>
    </div>
  </nav>
