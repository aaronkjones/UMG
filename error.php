<?php session_start()?>
<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Favicon -->
  <link rel="icon" href="images/favicon.ico" type="image/x-icon">

  <meta charset="UTF-8">

  <!-- If IE use the latest rendering engine -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- Set the page to the width of the device and set the zoon level -->
  <meta name="viewport" content="width = device-width, initial-scale = 1">
  <title>Uinta Medical Group</title>

  <!-- Bootstrap styling -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

  <!-- Fontawesome awesomeness -->
  <script src="https://use.fontawesome.com/7b7005c99f.js"></script>


  <!-- Custom styles for this template -->
  <link href="https://getbootstrap.com/examples/sticky-footer-navbar/sticky-footer-navbar.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/sticky-footer-navbar.css" rel="stylesheet">
  <link href="css/fancy.css" rel="stylesheet">


</head>
<body>
  <!-- Top navbar -->
  <?php require_once('navbar.php');?>
  <div id="content-wrapper">
    <div class="container" style="padding: 0px">
      <h1 class="well">Uhh ohhh...</h1>
      <div class="col-lg-12 well" style="margin-bottom: 50px">
    <h2>
      <?php
      echo "Error: " .  $_SESSION['error'] . "<br>";
      echo "Message: " .  $_SESSION['message'] . "<br>";
      echo "User type: " . $_SESSION['usertype'] . "<br>";
      echo "User: " . $_SESSION['username'] . "<br>";

      ?><br></h2>
  </div>
</div>
  </div>
  <!-- Footer -->
  <?php require_once('footer.php');?>
</body>
</html>
