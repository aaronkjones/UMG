<?php
require_once('creds.php');

// Session management
session_start();

$_SESSION['message'] = '';

// Check user's info for session hijack prevention
if ($_SESSION['check'] != hash('ripemd128', $_SERVER['REMOTE_ADDR'] .
$_SERVER['HTTP_USER_AGENT']))
{
  header("location: index.php");
}
else {
  // Build mysql connection
  $db = new mysqli($db_host, $db_user, $db_pass, $db_database);

  // Connection error handling
  if ($db->connect_error){
    die("Oh, noooo...Connection to database failed! " . $db->connect_error);
  }

  $usertype = $_SESSION['usertype'];
  $un = $_SESSION['username'];

  if($usertype === 'patients')
  {
    // Query treatments for logged in user
    $sql = "SELECT * FROM treatments INNER JOIN patients ON treatments.patientID = patients.patientID WHERE patients.username = '$un'";
    $treatment = mysqli_query($db, $sql);
  }
  else if ($usertype === 'physicians')
  {
    $sql = "SELECT * FROM treatments INNER JOIN physicians ON treatments.physicianID = physicians.physicianID WHERE physicians.username = '$un'";
    $treatment = mysqli_query($db, $sql);
  }
  else if ($usertype === 'admins')
  {
    $sql = "SELECT * FROM treatments INNER JOIN physicians ON treatments.physicianID = physicians.physicianID INNER JOIN patients ON treatments.patientID = patients.patientID";
    $treatment = mysqli_query($db, $sql);
  }
  else
  {
    header("location: error.php");
  }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Favicon -->
  <link rel="icon" href="images/favicon.ico" type="image/x-icon" />

  <meta charset="UTF-8">

  <!-- If IE use the latest rendering engine -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- Set the page to the width of the device and set the zoon level -->
  <meta name="viewport" content="width = device-width, initial-scale = 1">
  <title>Uinta Medical Group</title>

  <!-- Bootstrap styling -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

  <!-- Fontawesome awesomeness -->
  <script src="https://use.fontawesome.com/7b7005c99f.js"></script>

  <!-- Google Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/sticky-footer-navbar.css" rel="stylesheet">
  <link href="css/fancy.css" rel="stylesheet">

</head>
<body>

  <!-- Top navbar -->
  <?php require_once('navbar.php');?>
  <div id="content-wrapper">
    <div class="container" style="padding:0px; margin-bottom: 50px">
      <div id="container">
        <h1 class="well">Current Treatments</h1>
        <div id="treatments">
          <!-- Include letter nav bar -->
          <?php include_once('lettersnav.php'); ?>
          <ul style="padding-left:0px">
            <?php while($row = mysqli_fetch_assoc($treatment)) : ?>
              <li class="treatments">
                <span><hr width="100%" NOSHADE align="left" style="height:3px"></span>
                <span><b><?php echo $row['treat_name'] ?><br></span></b>
                <span><hr width="100%" NOSHADE align="left" style="height:3px"></span>
                <span><b>Treatment ID</b>: <?php echo $row['treatmentID'] ?><br></span>
                <span><b>Start Date</b>: <?php echo $row['start_date'] ?><br></span>
                <span><b>End Date</b>: <?php echo $row['end_date'] ?><br></span>
                <span><b>Start Time</b>: <?php echo $row['start_time'] ?><br></span>
                <span><b>End Time</b>: <?php echo $row['end_time'] ?><br></span>
                <span><b>Physician ID</b>: <?php echo $row['physicianID'] ?><br></span>
                <span><b>Patient ID</b>: <?php echo $row['patientID'] ?><br></span>
                <span><b>Notes</b>:<br> <?php echo $row['notes'] ?><br></span>
                <br>
                <?php if ($usertype === 'admins') echo '<a href="updatetreatment.php" class="btn btn-sm btn-primary" role="button">Update</a>'?>
                <br>
              </li>
            <?php endwhile; mysqli_close($db);?>
          </ul>
        </div>
      </div>
    </div>
    <?php require_once('footer.php');?>
  </body>
</div>
</html>
