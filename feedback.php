<?php session_start()?>
<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="icon" href="images/favicon.ico" type="image/x-icon" />

  <meta charset="UTF-8">

  <!-- If IE use the latest rendering engine -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- Set the page to the width of the device and set the zoon level -->
  <meta name="viewport" content="width = device-width, initial-scale = 1">
  <title>Uinta Medical Group</title>

  <!-- Bootstrap styling -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  <!-- <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->
  <link rel="stylesheet" type="text/css" href="css/bootstrap.css">

  <!-- Fontawesome awesomeness -->
  <script src="https://use.fontawesome.com/7b7005c99f.js"></script>

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/sticky-footer-navbar.css" rel="stylesheet">
  <link href="css/fancy.css" rel="stylesheet">

  <!-- Custom javascript -->
  <script src="slide.js"></script>
  
  <style>


  div.page-header {
    font-family: oxygen;
    font-size: 48px;
    margin-top: 50px;
  }

  p {
    font-family: roboto;
  }

  /*Fix for mobile screens*/
  @media(max-width: 480px) {
    div.page-header {
      font-size: 18pt;
    }
  }

  </style>


</head>
<body>
  <?php require_once('navbar.php');?>
  <div class="container" height="100%"><!--style="padding:0px; margin-left:100px"-->
    <div class="page-header">
      <b><i class="fa fa-stethoscope" aria-hidden="true">&nbsp;</i>Feedback</b>
    </div>
    <br>
            <img alt="hmm interesting" src="images/cool.gif" height="100%" width="100%">
      </div>
    </div>
    <?php require_once('footer.php');?>
  </body>
  </html>
