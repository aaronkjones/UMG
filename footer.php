<nav class="navbar navbar-inverse navbar-fixed-bottom" style="padding:0; margin:0">
  <div class="container" style="padding:10px">
    <div class="row">
      <div class="col-sm-12">
        <a href="index.php">Home</a> -
        <a href="feedback.php">Feedback</a> -
        <a href="donations.php">Donations</a> -
        <a href="publicrelations.php">Public Relations</a>
        &nbsp;
        <a class="btn btn-social-icon btn-twitter" style="font-size:10px"><i class="fa fa-twitter"></i></a>
        <a class="btn btn-social-icon btn-linkedin" style="font-size:10px"><i class="fa fa-linkedin"></i></a>
        <a class="btn btn-social-icon btn-facebook" style="font-size:10px"><i class="fa fa-facebook"></i></a>
    </div>
  </div>
</div>
</nav>
