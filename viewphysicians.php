<?php

require_once('creds.php');

// Session management
session_start();

// Check user's info for session hijack prevention
if ($_SESSION['check'] != hash('ripemd128', $_SERVER['REMOTE_ADDR'] .
$_SERVER['HTTP_USER_AGENT']))
{
  header("location: index.php");
}
else {
  // Establish mysql connection
  $db = new mysqli($db_host, $db_user, $db_pass, $db_database);

  // Connection error handling
  if ($db->connect_error){
    die("Oh, noooo...Connection to database failed! " . $db->connect_error);
  }

  // Query physicians
  $sql = "SELECT * FROM physicians";
  $treatment = mysqli_query($db, $sql);

  // Make sure the form is being posted
  if($_SERVER['REQUEST_METHOD'] == 'POST')
  {
    $fname = $mysqli->real_escape_string($_POST['fname']);
    $lname = $mysqli->real_escape_string($_POST['lname']);
    $dob = $mysqli->real_escape_string($_POST['dob']);
    $blood = $mysqli->real_escape_string($_POST['blood']);
    $street = $mysqli->real_escape_string($_POST['street']);
    $city = $mysqli->real_escape_string($_POST['city']);
    $state = $mysqli->real_escape_string($_POST['state']);
    $zip = $mysqli->real_escape_string($_POST['zip']);
    $phone = $mysqli->real_escape_string($_POST['phone']);
    $poc = $mysqli->real_escape_string($_POST['poc']);
    $poc_phone = $mysqli->real_escape_string($_POST['poc_phone']);
    $_SESSION['name'] = $name;

    $sql = "INSERT INTO physicians (name, dob, blood, address, phone, poc) "
    . "VALUES ('$fname', '$lname', '$dob', '$blood', '$street', '$city', '$state', '$zip','$phone', '$poc', '$poc_phone')";
    if ($mysqli->query($sql)=== true){
      $_SESSION['message'] = 'Registration successful';
      //header("location: welcome.php");
    }
    else {
      $_SESSION['message'] = "User could not be added";
    }
  }
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Favicon -->
  <link rel="icon" href="images/favicon.ico" type="image/x-icon" />

  <meta charset="UTF-8">

  <!-- If IE use the latest rendering engine -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- Set the page to the width of the device and set the zoon level -->
  <meta name="viewport" content="width = device-width, initial-scale = 1">
  <title>Uinta Medical Group</title>

  <!-- Bootstrap styling -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

  <!-- Fontawesome awesomeness -->
  <script src="https://use.fontawesome.com/7b7005c99f.js"></script>

  <!-- Google Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Passion+One' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="css/sticky-footer-navbar.css" rel="stylesheet">
  <link href="css/fancy.css" rel="stylesheet">

</head>
<body>

  <!-- Top navbar -->
  <?php require_once('navbar.php');?>

  <div id="content-wrapper">

    <div class="container" style="padding:0px; margin-bottom: 50px">

      <h1 class="well">Current Physicians</h1>
      <div id="physicians">
        <!-- Include letter nav bar -->
        <?php include_once('lettersnav.php'); ?>
        <ul style="padding-left:0px">
          <?php while($row = mysqli_fetch_assoc($treatment)) : ?>
            <li class="physicians">
              <span><hr width="60%" NOSHADE align="left" style="height:3px"></span>
              <span><b><?php echo $row['lname'] ?>, <?php echo $row['fname'] ?><br></span></b>
              <span><hr width="60%" NOSHADE align="left" style="height:3px"></span>
              <span>Street: <?php echo $row['street'] ?><br></span>
              <span>City: <?php echo $row['city'] ?><br></span>
              <span>State: <?php echo $row['state'] ?><br></span>
              <span>Zip: <?php echo $row['zip'] ?><br></span>
              <span>Phone: <?php echo $row['phone'] ?><br></span>
              <span>Date of Birth: <?php echo $row['dob'] ?><br></span>
              <span>Blood: <?php echo $row['blood'] ?><br></span>
              <span>Emergency Contact: <?php echo $row['poc'] ?><br></span>
              <span>Phone: <?php echo $row['poc_phone'] ?><br></span>
              <span>Position: <?php echo $row['position'] ?><br></span>
              <span>Username: <?php echo $row['username'] ?><br></span>
              <br>
              <?php if ($usertype === 'admins') echo '<a href="updatephysician.php" class="btn btn-sm btn-primary" role="button">Update</a>'?>
              <br>
            </li>
          <?php endwhile; mysqli_close($db);?>
        </ul>
      </div>
    </div>
  </body>
</div>
<?php require_once('footer.php');?>
</html>
