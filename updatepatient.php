<?php

// Creds for MySQL connection
require_once 'creds.php';

// Session management
session_start();
$_SESSION['message'] = '';

// Check user's info for session hijack prevention
if ($_SESSION['check'] != hash('ripemd128', $_SERVER['REMOTE_ADDR'] .
$_SERVER['HTTP_USER_AGENT']))
{
  header("location: index.php");
}
else {

  // Establish mysql connection
  $db = new mysqli($db_host, $db_user, $db_pass, $db_database);

  // Connection error handling
  if ($db->connect_error)
  {
    die("<h2>Oh noooo...Connection to database failed!</h2> " . $db->connect_error);
  }

  // Make sure the form is being posted
  if($_SERVER['REQUEST_METHOD'] == 'POST')
  {
    $patientID = $db->real_escape_string($_POST['patientID']);
    $fname = $db->real_escape_string($_POST['fname']);
    $lname = $db->real_escape_string($_POST['lname']);
    $dob = $db->real_escape_string($_POST['dob']);
    $blood = $db->real_escape_string($_POST['blood']);
    $street = $db->real_escape_string($_POST['street']);
    $city = $db->real_escape_string($_POST['city']);
    $state = $db->real_escape_string($_POST['state']);
    $zip = $db->real_escape_string($_POST['zip']);
    $phone = $db->real_escape_string($_POST['phone']);
    $poc = $db->real_escape_string($_POST['poc']);
    $poc_phone = $db->real_escape_string($_POST['poc_phone']);
    $username = $db->real_escape_string($_POST['username']);
    $password = $db->real_escape_string($_POST['password']);

    if(isset($_POST['deleteRecord']))
    {
      $sql = "DELETE FROM `patients` WHERE `patientID` = '$patientID'";
      if ($db->query($sql) === TRUE)
      {
        $_SESSION['message'] = "Record successfully deleted";
        header("location: index.php");
      }
      // else
      // {
      //   $_SESSION['error'] = "Record could not be deleted";
      //   header("location: error.php");
      // }
    }

    // Salting for password security
    $salt1 = '%kdv@z';
    $salt2 = 'l1D30x';
    $token = hash('ripemd128',"$salt1$password$salt2");

    // $result = $db->query("SELECT * FROM patients WHERE fname='$fname' AND lname='$lname'") or die($mysqli->error());

    // // Patient exists if the rows returned are more than 0
    // if ( $result->num_rows <= 0 ) {
    //
    //   $_SESSION['error'] = 'Patient ' . $fname . ' ' . $lname . ' does not exist. Please add them first.';
    //   header("location: error.php");
    // }

    // Patient does exist in a database, proceed...
    // else
    // {
      $sql = "UPDATE patients SET fname = '$fname', lname = '$lname', dob = '$dob', blood = '$blood', street = '$street', city = '$city', state = '$state', zip = $zip, phone = '$phone', poc = '$poc', poc_phone = '$poc_phone', username = '$username', password = '$token' WHERE fname = '$fname' AND lname = '$lname' AND patientID = '$patientID'" or die($db->error);
      if ($db->query($sql) === TRUE)
      {
        $_SESSION['message'] = 'Record update successful';
      }
      else
      {
        $_SESSION['message'] = "There was an error adding the patient<br>" . mysqli_error($db);
      }
    // }
  }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Favicon -->
  <link rel="icon" href="images/favicon.ico" type="image/x-icon">

  <meta charset="UTF-8">

  <!-- If IE use the latest rendering engine -->
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- Set the page to the width of the device and set the zoon level -->
  <meta name="viewport" content="width = device-width, initial-scale = 1">
  <title>Uinta Medical Group</title>

  <!-- Bootstrap styling -->
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
  <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

  <!-- Fontawesome awesomeness -->
  <script src="https://use.fontawesome.com/7b7005c99f.js"></script>

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link href='https://fonts.googleapis.com/css?family=Oxygen' rel='stylesheet' type='text/css'>

  <!-- Custom styles for this template -->
  <link href="https://getbootstrap.com/examples/sticky-footer-navbar/sticky-footer-navbar.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/sticky-footer-navbar.css" rel="stylesheet">
  <link href="css/fancy.css" rel="stylesheet">

  <!-- Custom javascript -->
  <script src="slide.js"></script>

</head>
<body>

  <!-- Top navbar -->
  <?php require_once('navbar.php');?>

  <div id="content-wrapper">
    <div class="container" style="padding: 0px">
      <h1 class="well">Update Patient</h1>
      <div class="col-lg-12 well" style="margin-bottom: 50px">
        <div class="row">

          <!-- Form -->
          <form action="updatepatient.php" method="post">
            <div class="col-sm-12">

              <!-- Row -->
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>First Name</label>
                  <select class="form-control" name="fname">
                    <option value = "">---Select---</option>
                    <?php
                    // Get patients ID, first name, last name, and sort alphabetically by last name
                    $query = mysqli_query($db, "SELECT `fname`,`lname` FROM `patients` ORDER BY 'lname'");
                    while ( $d=mysqli_fetch_assoc($query)) {
                      echo "<option value='"  . $d['fname'] . "'>" . $d['fname'] . " (" . $d['lname'] . ", " . $d['fname'] . ")" . "</option>";
                    }
                    ?>
                  </select>
                </div>
                <div class="col-sm-4 form-group">
                  <label>Last Name</label>
                  <select class="form-control" name="lname">
                    <option value = "">---Select---</option>
                    <?php
                    // Get patients ID, first name, last name, and sort alphabetically by last name
                    $query = mysqli_query($db, "SELECT `fname`,`lname` FROM `patients` ORDER BY 'lname'");
                    while ( $d=mysqli_fetch_assoc($query)) {
                      echo "<option value='"  . $d['lname'] . "'>" . $d['lname'] . " (" . $d['lname'] . ", " . $d['fname'] . ")" . "</option>";
                    }
                    ?>
                  </select>
                </div>
                <div class="col-sm-4 form-group">
                  <label>Patient ID</label>
                  <select class="form-control" name="patientID" required>
                    <option value = "">---Select---</option>
                    <?php
                    // Get patients ID, first name, last name, and sort alphabetically by last name
                    $query = mysqli_query($db, "SELECT `fname`,`lname`, `patientID` FROM `patients` ORDER BY 'lname'");
                    while ( $d=mysqli_fetch_assoc($query)) {
                      echo "<option value='"  . $d['patientID'] . "'>" . $d['patientID'] . " (" . $d['lname'] . ", " . $d['fname'] . ")" . "</option>";
                    }
                    ?>
                  </select>
                </div>
              </div>
              <hr style="border-color:#e5e5e5">
              <!-- Row -->
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>Street</label>
                  <input type="text" placeholder="Enter street" class="form-control" name="street">
                </div>
                <div class="col-sm-4 form-group">
                  <label>City</label>
                  <input type="text" placeholder="Enter city" class="form-control" name="city">
                </div>
                <div class="col-sm-4 form-group">
                  <label>State</label>
                  <input type="text" placeholder="UT" class="form-control" name="state">
                </div>
              </div>

              <!-- Row -->
              <div class="row">
                <div class="col-sm-2 form-group">
                  <label>Zip</label>
                  <input type="text" placeholder="Zip code" class="form-control" name="zip">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Phone</label>
                  <input type="text" placeholder="(xxx)xxx-xxxx" class="form-control" name="phone">
                </div>
              </div>
              <hr style="border-color:#e5e5e5">

              <!-- Row -->
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>Date of birth</label>
                  <input type="date" class="form-control" name="dob">
                </div>

                <div class="col-sm-2 form-group">
                  <label>Blood type</label>
                  <select id="bloodtype" class="form-control" name="blood">
                    <option value="A-">A-</option>
                    <option value="A+">A+</option>
                    <option value="B-">B-</option>
                    <option value="B+">B+</option>
                    <option value="AB-">AB-</option>
                    <option value="AB+">AB+</option>
                    <option value="O-">O-</option>
                    <option value="O+">O+</option>
                  </select>
                </div>
              </div>

              <!-- Row -->
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>Emergency contact</label>
                  <input type="text" placeholder="Emergency contact" class="form-control" name="poc">
                </div>

                <div class="col-sm-4 form-group">
                  <label>Contact phone</label>
                  <input type="text" placeholder="(xxx)xxx-xxxx" class="form-control" name="poc_phone">
                </div>
              </div>
              <hr style="border-color:#e5e5e5">

              <!-- Row -->
              <div class="row">
                <div class="col-sm-4 form-group">
                  <label>Username</label>
                  <input type="text" placeholder="jdoe" class="form-control" name="username">
                </div>
                <div class="col-sm-4 form-group">
                  <label>Password</label>
                  <input type="text" placeholder="secretsss" class="form-control" name="password">
                </div>
              </div>
              <div class="container" style="padding: 0px">
                <h2><?php echo $_SESSION['message'];?><br></h2>
              </div>

              <hr style="border-color:#e5e5e5">
              <button type="submit" class="btn btn-primary">Submit</button>
              <button type="submit" name="deleteRecord" class="btn btn-danger pull-right">Delete</button>
              <?php ?>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!-- Footer -->
    <?php require_once('footer.php');?>
    <?php mysqli_close($db);?>
  </body>
</div>
</html>
