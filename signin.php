<?php

session_start();

// If user is already logged in, send them to the main page
if( isset($_SESSION['username']) ){
  header("Location: index.php");
}

// Creds for MySQL connection
require_once 'creds.php';

// Build mysql connection
$db = new mysqli($db_host, $db_user, $db_pass, $db_database);

// Connection error handling
if ($db->connect_error)
{
  die("<h2>Oh noooo...Connection to database failed!</h2> " . $db->connect_error);
}

// If form is submitted
if (isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']))
{
  // Assign supplied user and pw to tmp variables
  $tmp_username = mysql_entities_fix_string($db, isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : '');
  $tmp_password = mysql_entities_fix_string($db, isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_PW'] : '');

  // Establish user type (e.g. patient, physician, etc)
  $_SESSION['usertype'] = $_GET['type'];
  $usertype = $_SESSION['usertype'];

  // Build sql query
  $query = "SELECT password FROM $usertype WHERE username = '$tmp_username'";

  // Execute query
  $result = $db->query($query);

  // If there is an error die
  if($db->connect_error)
  {
    $_SESSION['error'] = $db->error;
    header("location:error.php");
  }
  // Count number of rows in the table (number of patients)
  $rows = $result->num_rows;

  $password = '';

  // For each row, passing value for password to password variable
  for($j=0;$j<$rows;$j++)
  {
    $result->data_seek($j);
    $row = $result->fetch_array(MYSQLI_ASSOC);
    $password = $row['password'];
  }

  // Salting password for security
  $salt1 = '%kdv@z';
  $salt2 = 'l1D30x';
  $token = hash('ripemd128',"$salt1$tmp_password$salt2");


  // If supplied password matches password from the database, begin session and add tmp un and pw to session array
  if($token == $password){
    session_start();
    $_SESSION['username'] = $tmp_username;
    $_SESSION['password'] = $tmp_password;
    $_SESSION['loggedin'] = true;

    // Get user's info for session hijack prevention
    $_SESSION['check'] = hash('ripemd128', $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT']);

    if ($usertype === 'patients')
    header("location:viewtreatments.php");
    elseif ($usertype === 'physicians')
    header("location:viewpatients.php");
    elseif ($usertype === 'admins')
    header("location:index.php");
    else
    {
    $_SESSION['error'] = "Cannot determine user type.";
    header("location: error.php");
  }
  }

  else{
    $_SESSION['error'] = $db->error;
    header("location:error.php");
  }
}
else{

  header('WWW-Authenticate: Basic realm="My Realm"');
  header('HTTP/1.0 401 Unauthorized');
  print_r(isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_USER'] : '');
  print_r(isset($_SERVER['PHP_AUTH_USER']) ? $_SERVER['PHP_AUTH_PW'] : '');
  die("Please enter your username and password<br>");

}

//sanitization functions
function mysql_entities_fix_string($conn, $string){
  return htmlentities(mysql_fix_string($conn, $string));
}

function mysql_fix_string($conn, $string){
  if(get_magic_quotes_gpc()) $string = stripslashes($string);
  return $conn->real_escape_string($string);
}

?>
